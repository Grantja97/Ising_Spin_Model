# -*- coding: utf-8 -*-
"""
Created on Mon Jan 01 22:25:36 2018

@author: Jamie Grant
"""

# Simulating the Ising model
from __future__ import division
import numpy as np
from numpy.random import rand
import matplotlib.pyplot as plt


class Ising():
    ''' Simulating the Ising model '''    
    ## Monte Carlo sweeping over lattice
    def sweep(self, spin, N, beta):
        
        
        for i in range(N):
            for j in range(N):            
                    a = np.random.randint(0, N)
                    b = np.random.randint(0, N)
                    s =  spin[a, b]
                    nb = spin[(a+1)%N,b] + spin[a,(b+1)%N]
                    + spin[(a-1)%N,b] + spin[a,(b-1)%N]
                    cost = 2*s*nb
                    if cost < 0:	
                        s *= -1
                    elif rand() < np.exp(-cost*beta):
                        s *= -1
                    spin[a, b] = s
        return spin
    
    def Ising_simulate(self):   
        
        N = 50
        temp = 0                                                               # Initialse the lattice
        spin = 2*np.random.randint(2, size=(N,N))-1
        f = plt.figure(figsize=(15, 15));    
        self.spinPlot(f, spin, 0, N, 1);
        
        sweep = 1001
        for i in range(sweep):
            self.mcmove(spin, N, 1.0/temp)
            if i == 1:        self.configPlot(f, spin, i, N, 2);
            if i == 4:        self.configPlot(f, spin, i, N, 3);
            if i == 32:       self.configPlot(f, spin, i, N, 4);
            if i == 100:      self.configPlot(f, spin, i, N, 5);
            if i == 1000:     self.configPlot(f, spin, i, N, 6);
                 
                    
    def spinPlot(self, f, spin, i, N, n_):
        X, Y = np.meshgrid(range(N), range(N))
        sp =  f.add_subplot(3, 3, n_ )  
        plt.setp(sp.get_yticklabels(), visible=False)
        plt.setp(sp.get_xticklabels(), visible=False)      
        plt.pcolormesh(X, Y, spin, cmap=plt.cm.RdBu);
        plt.title('Time=%d'%i); plt.axis('tight')    
    plt.show()
    
