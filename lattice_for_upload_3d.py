# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 01:53:39 2018

@author: Jamie Grant
"""

#Assigning packages to be used in the script
from __future__ import division
import numpy as np
from numpy.random import rand
import matplotlib.pyplot as plt

#______________________________________________________________________________

#Defining functions for Ising Model Simulation


def spin_config3d(N):                                                          #random spin configuration for initial step
   
    state=2*np.random.randint(2,size=(N,M,P))-1                                #Creating random values
    return state


def monte_metro3d(spin,b):                                                      #Using metropolis_Hastings
#                                                                               algorithm for the monte carlo method
     
    for i in range(N):                                                         
        for j in range(N):                                                     
            for k in range(N):                                                 
                x=np.random.randint(0,N)                                       #random integer values in x,y,z assigned to 
                y=np.random.randint(0,M)                                       #each spin site in lattice
                z=np.random.randint(0,P)                                       
                r=spin[x,y,z]                                                  #assigning a spin configuration to each of this
                a=spin[(x+1)%N,y,z]+spin[(x-1)%N,y,z]+spin[x,(y+1)%M,z]        #cartesian points
                +spin[x,(y-1)%M,z]+spin[x,y,(z+1)%P]+spin[x,y,(z-1)%P]
                                                                               #ensuring contribution of the nearest neighbour
                value=2*r*a                                                    #is counted when sweeping over lattice
                if value<0:
                        
                    r*=-1
                elif rand() < np.exp(-value*b):   
                    r*=-1
                spin[x,y,z]=r
    return spin

#Originally attempted to constructed 3D toroidal lattice 

#def lattice_control(cry,xyz,T,k,Q):
 #   for i in range(k):
 #       x = choice(range(xyz))
 #       y = choice(range(xyz))
 #       z = choice(range(xyz))
        
 #       x_right  =  x + 1 
 #       x_left   =  x - 1
 #       y_above  =  y + 1 
 #       y_below  =  y - 1
 #       z_front  =  z + 1 
 #       z_behind =  z - 1 
        
 #       if x_right == xyz:            #so that the lattice sites at the 
 #           x_right = 0               #boundary would not be isolated in
 #       if x_left == xyz:             #a particular direction
 #           x_left = 0 
 #       if y_above == xyz:
 #           y_above = 0 
 #       if y_below == xyz:
 #           y_below = 0 
 #       if z_front == xyz:
 #           z_front = 0 
 #       if z_behind == xyz:
 #           z_behind = 0 
 
#______________________________________________________________________________ 
#Uncomment for 2D analgoue to the above definitions
#______________________________________________________________________________
#def super_cell(N):   
#    ''' generating a super cell of random spin configurations, either up/down'''
    
#    spin_state = 2*np.random.randint(2, size=(N,N))-1       #lattice dimentions NxN as its a simple
#    return spin_state                                       # square lattice


#def mcmove(spin, p):
#   '''Using the Metropolis-Hasting's Alhorithm for the Monte Carlo Method as 
#        sample set consists of bivaritae data'''
#    for i in range(N):
#        for j in range(N):
#                a = np.random.randint(0, N)
#                b = np.random.randint(0, N)
#                s =  spin[a, b]
#                nb = spin[(a+1)%N,b] + spin[a,(b+1)%N]
#                + spin[(a-1)%N,b] + spin[a,(b-1)%N]  
#                u = 2*s*nb
#                if u < 0:
#                    s *= -1
#                elif rand() < np.exp(-u*p):
#                    s *= -1
#                spin[a, b] = s
#    return config         

  
def energy_func(spin):
                                                                               #Energy of a given configuration
    energy=0                                                                   #Setting initial value
    for i in range(len(spin)):                                                 #augment energy of each
        for j in range(len(spin)):                                             #lattice spin site
            for k in range(len(spin)):
                c = spin[i,j,k]  
                d = spin[(i+1)%N,j,k]+spin[(i-1)%N,j,k]
                +spin[i,(j+1)%M,k]+spin[i,(j-1)%M,k]
                +spin[i,j,(k+1)%P]+spin[i,j,(k-1)%P]
                energy+=-d*c                                                   #increasing energy per iteration
    return energy/2                                                            #returning new energy


def magnet_func(spin):
                                                                               #Magnetization of a given spin configuration
    mag=np.sum(spin)                                                           #Summation of global spin state average
    return mag                                                                 #value will define the magnestisation
                                                                               

#______________________________________________________________________________
#Uncomment for 2D analgoue to above fucntions
#______________________________________________________________________________
# def energy_func(spin):
#    '''Energy of a given configuration'''
#    energy = 0
#    for i in range(len(spin)):
#        for j in range(len(spin)):
#            Sample = spin[i,j]
#            nb = spin[(i+1)%N, j] + spin[i,(j+1)%N] 
#            + spin[(i-1)%N, j] + spin[i,(j-1)%N]
#            energy += nb*Sample*(-1)
#    return energy


#def calcMag(spin):
#    '''Magnetization of a given spin state'''
#    mag = np.sum(spin)
#    return mag   

nt=50                                                                             
N=10    
M=10                                                                           #Defining number of sweeps to 
P=10                                                                           #be carried out over the 50 
Equal_steps=1000                                                               #temperature points
Steps=1000                                                                     

n_1,n_2= 1.0/(Steps*N**3),1.0/(Steps*Steps*N**3)  
tm=2.27   
T=np.random.normal(tm, 2, nt)
T  = T[(T>0) & (T<10.0)]       
nT=np.size(T)


Energy= np.zeros(nT);   
Magnetization=np.zeros(nT)
SpecificHeat=np.zeros(nT)
Susceptibility=np.zeros(nT)


for m in range(len(T)):
    E_1=M_1=E_2=M_2=0                                                          
    shape=spin_config3d(N)
    iT=1.0/(T[m])
    iT2=iT*iT
    
    for i in range(Equal_steps):                                               
        monte_metro3d(shape, iT)                                               

    for i in range(Steps):
        monte_metro3d(shape, iT)           
        Energy_0=energy_func(shape)                                            
        Magnetic_0=magnet_func(shape)                                          

        E_1=E_1+Energy_0
        E_2=E_2+Energy_0**2                                                    
        
        
        M_1=M_1+Magnetic_0
        M_2=M_2+Magnetic_0**2                                                  
        

        Energy[m]         =n_1*E_1
        Magnetization[m]  =n_1*M_1
        SpecificHeat[m]   =(n_1*E_2 - n_2*(E_1**2))*iT2                        #adding first and second order terms of
        Susceptibility[m] =(n_1*M_2 - n_2*(M_1**2))*iT                         #Cv taylor expansion about Tc
#______________________________________________________________________________
#Plotting The Energy, Magnetization, SPecificHeat and mag.Susceptibility funcs.
#______________________________________________________________________________        
plt.figure(1)
plt.plot(T, Energy, 'o', color='green')
plt.title("Energy vs Temperature",fontsize=22)
plt.xlabel("Temperature", fontsize=20)
plt.ylabel("Energy ", fontsize=20)


plt.figure(2)
plt.plot(T, abs(Magnetization), 'o', color='red')
plt.title(" Magnetisation vs Temperature",fontsize=22)
plt.xlabel("Temperature", fontsize=20)
plt.ylabel("Magnetisation ", fontsize=20)

plt.figure(3)
plt.plot(T, SpecificHeat, 'o', color='blue')
plt.title("Specific Heat vs Temperature",fontsize=22)
plt.xlabel("Temperature", fontsize=20)
plt.ylabel("Specific Heat ", fontsize=20)

plt.figure(4)
plt.plot(T, Susceptibility, 'o', color='purple')
plt.title("Magnetic Susceptibility vs Temperature",fontsize=22)
plt.xlabel("Temperature", fontsize=20)
plt.ylabel("Magnetic Susceptibility", fontsize=20)